const postcssNested = require('postcss-nested')
const tailwindcss = require('tailwindcss')

module.exports = () => ({
  plugins: [
    postcssNested,
    tailwindcss,
  ],
})
