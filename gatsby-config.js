module.exports = {
  pathPrefix: '/egoiste-velvet.ru',
  siteMetadata: {
    sections: {
      main: {
        en: {
          buy: 'Buy',
        },
        ru: {
          buy: 'Купить',
        },
      },
      grainAndGround: {
        en: {
          sectionName: 'О бренде EGOISTE Velvet',
          title: 'EGOISTE Velvet молотый и зерновой',
          desc:
            'Тщательно отобранные зерна премиальной арабики, обжаренные до&nbsp;цвета молочного шоколада в&nbsp;обволакивающих потоках горячего воздуха, делают вкус кофе EGOISTE Velvet исключительно мягким и&nbsp;бархатистым.',
        },
        ru: {
          sectionName: 'О бренде EGOISTE Velvet',
          title: 'EGOISTE Velvet молотый и зерновой',
          desc:
            'Тщательно отобранные зерна премиальной арабики, обжаренные до&nbsp;цвета молочного шоколада в&nbsp;обволакивающих потоках горячего воздуха, делают вкус кофе EGOISTE Velvet исключительно мягким и&nbsp;бархатистым.',
        },
      },
      instantCoffeeSequence: {
        en: {
          sectionName: 'Молотый EGOISTE Velvet',
        },
        ru: {
          sectionName: 'Молотый EGOISTE Velvet',
        },
      },
      quote: {
        en: {
          title:
            'Изысканная смесь кофе из ароматных сортов 100% Арабики, со вкусом спелых ягод голубики и ежевики, с нежным фруктовым послевкусием, переходящим ко вкусу молочного шоколада.',
          desc:
            'Глубокие винные ноты, фруктовое послевкусие идеальный выбор для тех, кто предпочитает яркий, но деликатный вкус кофе без горчинки.',
        },
        ru: {
          title:
            'Изысканная смесь кофе из ароматных сортов 100% Арабики, со вкусом спелых ягод голубики и ежевики, с нежным фруктовым послевкусием, переходящим ко вкусу молочного шоколада.',
          desc:
            'Глубокие винные ноты, фруктовое послевкусие идеальный выбор для тех, кто предпочитает яркий, но деликатный вкус кофе без горчинки.',
        },
      },
      groundCoffeeSequence: {
        en: {
          sectionName: 'Зерновой  EGOISTE Velvet',
        },
        ru: {
          sectionName: 'Зерновой  EGOISTE Velvet',
        },
      },
      groundAndGrainCharacteristics: {
        en: {
          title: 'Характеристики EGOISTE Velvet молотый и&nbsp;зерновой',
          structure: {
            key: 'Состав',
            value: '100% Спешалити Арабика',
          },
          region: {
            key: 'Регион происхождения',
            value: 'Эфиопия (Сидамо), Перу',
          },
          treatment: {
            key: 'обработка',
            value: 'Мытая Арабика / Ручной сбор / Высокогорная сушка под солнцем',
          },
          roasting: {
            key: 'обжарка',
            value: 'City',
          },
          production: {
            key: 'производство',
            value: 'Германия',
          },
          desc: {
            key: 'Описание',
            value:
              'Насыщенный, восхитительный вкус с глубокими винными нотами и нежным фруктовым послевкусием.',
          },
        },
        ru: {
          title: 'Характеристики EGOISTE Velvet молотый и зерновой',
          structure: {
            key: 'Состав',
            value: '100% Спешалити Арабика',
          },
          region: {
            key: 'Регион происхождения',
            value: 'Эфиопия (Сидамо), Перу',
          },
          treatment: {
            key: 'обработка',
            value: 'Мытая Арабика / Ручной сбор / Высокогорная сушка под солнцем',
          },
          roasting: {
            key: 'обжарка',
            value: 'City',
          },
          production: {
            key: 'производство',
            value: 'Германия',
          },
          desc: {
            key: 'Описание',
            value:
              'Насыщенный, восхитительный вкус с глубокими винными нотами и нежным фруктовым послевкусием.',
          },
        },
      },
      instant: {
        en: {
          sectionName: 'Растворимый  EGOISTE Velvet',
          title: 'EGOISTE Velvet растворимый',
        },
        ru: {
          sectionName: 'Растворимый  EGOISTE Velvet',
          title: 'EGOISTE Velvet растворимый',
        },
      },
      instantCharacteristics: {
        en: {
          title: 'характеристики EGOISTE Velvet растворимый',
          structure: {
            key: 'Состав',
            value: '100% Спешалити Арабика',
          },
          region: {
            key: 'Регион происхождения',
            value: 'Центральная Америка, Колумбия',
          },
          treatment: {
            key: 'обработка',
            value: 'Мытая Арабика / Высокогорная сушка под солнцем',
          },
          roasting: {
            key: 'обжарка',
            value: 'City',
          },
          production: {
            key: 'производство',
            value: 'Швейцария',
          },
          desc: {
            key: 'Описание',
            value: 'Бархатный и деликатный, вкус этого кофе со сладким фруктовым послевкусием.',
          },
        },
        ru: {
          title: 'характеристики EGOISTE Velvet растворимый',
          structure: {
            key: 'Состав',
            value: '100% Спешалити Арабика',
          },
          region: {
            key: 'Регион происхождения',
            value: 'Центральная Америка, Колумбия',
          },
          treatment: {
            key: 'обработка',
            value: 'Мытая Арабика / Высокогорная сушка под солнцем',
          },
          roasting: {
            key: 'обжарка',
            value: 'City',
          },
          production: {
            key: 'производство',
            value: 'Швейцария',
          },
          desc: {
            key: 'Описание',
            value: 'Бархатный и деликатный, вкус этого кофе со сладким фруктовым послевкусием.',
          },
        },
      },
      partners: {
        en: {
          sectionName: 'Наши партнеры',
          title: 'приобрести EGOISTE Velvet можно в магазинах или заказать на дом',
        },
        ru: {
          sectionName: 'Наши партнеры',
          title: 'приобрести EGOISTE Velvet можно в магазинах или заказать на дом',
        },
      },
      cookies: {
        en: {
          desk:
            'Наш сайт использует файлы Cookies, чтобы Вам было удобнее им пользоваться. Всю подробную информацию о Cookies можно прочитать можно прочитать',
          info: 'здесь',
          button: 'Принять',
        },
        ru: {
          desk:
            'Наш сайт использует файлы Cookies, чтобы Вам было удобнее им пользоваться. Всю подробную информацию о Cookies можно прочитать можно прочитать',
          info: 'здесь',
          button: 'Принять',
        },
      },
      footer: {
        en: {
          questions: 'По всем вопросам',
          termOfUse: 'Пользовательское соглашение',
        },
        ru: {
          questions: 'По всем вопросам',
          termOfUse: 'Пользовательское соглашение',
        },
      },
    },
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyDefault: 'ru',
        useLangKeyLayout: false,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Egoiste Velvet`,
        // short_name: `starter`,
        // start_url: `/`,
        // background_color: `#663399`,
        // theme_color: `#663399`,
        // display: `minimal-ui`,
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        // Setting a color is optional.
        color: `tomato`,
        // Disable the loading spinner.
        showSpinner: false,
      },
    },
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true, // Print removed selectors and processed file names
        develop: false, // Enable while using `gatsby develop`
        tailwind: true, // Enable tailwindcss support
        // whitelist: ['whitelist'], // Don't remove this selector
        ignore: [
          '/instant-sequence.css',
          '/ground-sequence.css',
          '/slider.css',
          '/assets/index.css',
        ], // Ignore files/folders
        // purgeOnly : ['components/', '/main.css', 'bootstrap/'], // Purge only these files/folders
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
