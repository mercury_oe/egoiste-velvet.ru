import React, { forwardRef, useContext, useEffect, useImperativeHandle, useRef } from 'react';
import ScrollMagic from 'scrollmagic-with-ssr';
import { Linear, TimelineMax, TweenMax } from 'gsap';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ScrollMagicPluginGsap } from 'scrollmagic-plugin-gsap';
import useLoadImages from '../utils/useLoadImages';
import { Context } from './Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;
const controller1 = ScrollMagic ? new ScrollMagic.Controller() : null;
if (ScrollMagic) {
  ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);
}
const Sequence = forwardRef((props, r) => {
  const { images, className, onEnd, tastes } = props;
  const section = useRef(null);
  const ref = useRef(null);

  const itemClassName = `${className}-item`;
  const context = useContext(Context);
  const { setProgress, setLength } = context;
  const imgsIsLoaded = useLoadImages(images, setProgress);

  useEffect(() => {
    setLength(value => value + images.length);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useImperativeHandle(r, () => ({
    ...section,
  }));

  useEffect(() => {
    if (section && controller && imgsIsLoaded) {
      const offset = document.documentElement.clientHeight / 2;
      const duration = section.current.offsetHeight * 2;
      const offsetValue = duration / images.length;
      const mainScene = new ScrollMagic.Scene({
        triggerElement: `.${className}`,
        offset,
        duration: tastes ? duration * 4 : duration + 500,
      })
        .setPin(`.${className}`)
        .addTo(controller);

      mainScene.on('end', event => {
        if (onEnd) {
          onEnd(event);
        }
      });

      images.forEach((_, i) => {
        const scene = new ScrollMagic.Scene({
          triggerElement: `.${className}`,
          offset: offset + offsetValue + i * offsetValue,
        }).addTo(controller);
        scene.on('enter', () => {
          ref.current.setAttribute('src', images[i]);
        });
        scene.on('leave', () => {
          ref.current.setAttribute('src', images[i]);
        });
      });

      if (tastes) {
        const tween = TweenMax.to('#square', 1, { top: '100%', ease: Linear.easeNone });

        new ScrollMagic.Scene({
          triggerElement: `.${className}`,
          offset: offset + images.length * offsetValue,
          duration: duration * 2.5 + 500,
        })
          .setTween(tween)
          .addTo(controller1);
      }
    }
  }, [className, images, imgsIsLoaded, onEnd, tastes]);

  return (
    <section
      ref={section}
      className={classNames('w-screen h-screen relative overflow-hidden', className)}
    >
      <div
        id="square"
        className="w-full h-full tastes absolute"
        style={{ position: 'absolute', top: '-100%' }}
      />
      <img
        ref={ref}
        src={images[0]}
        className={classNames('w-full h-full', itemClassName)}
        alt="sequence"
      />
    </section>
  );
});

Sequence.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
  className: PropTypes.string.isRequired,
  onEnd: PropTypes.func,
  tastes: PropTypes.bool,
};

Sequence.defaultProps = {
  onEnd: null,
  tastes: false,
};

export default Sequence;
