import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { Link } from 'gatsby';

const KEYS = {
  EN: '/en',
  RU: '/',
};

export default function LanguageToggle({ className }) {
  let isEn = false;
  if (typeof window !== 'undefined') {
    isEn = window.location.href.includes('/en');
  }

  return (
    <div className={classNames('language-toggle', className)}>
      <LanguageButton
        value={KEYS.RU}
        className={classNames('ru mr-3', {
          'text-white': !isEn,
          'opacity-100': !isEn,
        })}
      >
        RU
      </LanguageButton>
      <LanguageButton
        value={KEYS.EN}
        className={classNames('en', {
          'text-white': isEn,
          'opacity-100': isEn,
        })}
      >
        EN
      </LanguageButton>
    </div>
  );
}

LanguageToggle.propTypes = {
  className: PropTypes.string,
};

LanguageToggle.defaultProps = {
  className: '',
};

const LanguageButton = ({ value, className, children }) => {
  return (
    <Link
      className={classNames(
        'outline-none leading-none text-tiny text-brown-lighter opacity-50',
        className,
      )}
      to={value}
    >
      {children}
    </Link>
  );
};

LanguageButton.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  value: PropTypes.string,
};

LanguageButton.defaultProps = {
  children: null,
  className: '',
  value: '',
};
