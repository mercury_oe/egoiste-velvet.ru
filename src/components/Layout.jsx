import React, { createContext, useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../styles/main.css';
import { useIsMobile } from '../utils/responsiveUtils';

export const Context = createContext({
  loaded: false,
  length: 0,
  progress: 0,
  isLoaded: false,
});

const Layout = ({ children }) => {
  const isMobile = useIsMobile();
  const [currentSection, setCurrentSection] = useState({ title: '' });
  const [progress, setProgress] = useState(0);
  const [loaded, setLoaded] = useState(false);
  const [length, setLength] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);

  const handleProgress = useCallback(() => {
    setProgress(prevState => prevState + 1);
  }, []);

  useEffect(() => {
    setIsLoaded(progress === length);
  }, [length, progress]);

  return (
    <Context.Provider
      value={{
        progress,
        setProgress: handleProgress,
        loaded,
        setLoaded,
        length,
        setLength,
        isLoaded,
        currentSection,
        setCurrentSection,
      }}
    >
      <main
        className={classNames('w-full bg-gray-light', {
          'overflow-hidden': isMobile ? false : !isLoaded,
          'h-screen': isMobile ? false : !isLoaded,
        })}
      >
        {children}
      </main>
    </Context.Provider>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
