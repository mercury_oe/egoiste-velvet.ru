import PropTypes from 'prop-types';
import React from 'react';

export default function SliderDot({ className }) {
  return (
    <svg className={className} viewBox="0 0 37 37">
      <g fill="none" fillRule="evenodd">
        <circle cx="18.49" cy="18.49" r="5.69" fill="#FFFFFF" fillRule="nonzero" />
        <path
          stroke="#FFFFFF"
          strokeDasharray="5"
          d="M18.13 35.77a17.63 17.63 0 1 0 0-35.27 17.63 17.63 0 0 0 0 35.27z"
        />
      </g>
    </svg>
  );
}

SliderDot.propTypes = {
  className: PropTypes.string,
};

SliderDot.defaultProps = {
  className: '',
};
