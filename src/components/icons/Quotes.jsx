import PropTypes from 'prop-types';
import React from 'react';

const Quotes = ({ width, height, className }) => {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 75 48">
      <path
        fill="#D1C2B3"
        d="M24.27 0v3.98c-6.37 3.83-10.71 7.39-13.03 10.66a14.49 14.49 0 0 0-2.53 8.53c0 1.97.38 3.42 1.13 4.35.69.93 1.5 1.4 2.43 1.4.63 0 1.5-.23 2.63-.66 1.68-.55 3.06-.83 4.12-.83 2.5 0 4.72.93 6.65 2.78a9.22 9.22 0 0 1 2.9 6.95c0 3.15-.96 5.59-2.9 7.32A13.8 13.8 0 0 1 16.3 48c-4.24 0-8.02-1.76-11.33-5.28C1.66 39.2 0 34.84 0 29.65c0-7.29 2.75-13.93 8.25-19.92 3.68-3.95 9.02-7.2 16.02-9.73zm26.37 48v-4.36c6.43-3.76 10.8-7.28 13.12-10.56a14.45 14.45 0 0 0 2.43-8.43c0-1.92-.34-3.34-1.03-4.26-.75-.93-1.56-1.4-2.43-1.4-.7 0-1.6.22-2.72.65a13 13 0 0 1-4.03.84c-2.5 0-4.72-.93-6.65-2.78a9.22 9.22 0 0 1-2.9-6.95c0-3.09.96-5.5 2.9-7.23A13.58 13.58 0 0 1 58.7 0c4.18 0 7.93 1.76 11.24 5.28C73.31 8.74 75 13.07 75 18.25c0 7.3-2.78 13.94-8.34 19.93-3.68 3.95-9.03 7.23-16.02 9.82z"
      />
    </svg>
  );
};

Quotes.propTypes = {
  className: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Quotes.defaultProps = {
  className: '',
  height: 48,
  width: 75,
};

export default Quotes;
