import React from 'react';
import PropTypes from 'prop-types';

const Shop = ({ fill, className, onClick }) => {
  return (
    <svg onClick={onClick} className={className} viewBox="0 0 61 46">
      <g fill={fill}>
        <rect width="3.12" height="45.25" x="34.73" rx="1.56" />
        <rect width="3.12" height="32.62" x="46.3" y="6.31" rx="1.56" />
        <rect width="3.12" height="14.73" x="23.15" y="15.79" rx="1.56" />
        <rect width="3.12" height="14.73" x="57.88" y="15.79" rx="1.56" />
        <rect width="3.12" height="14.73" y="15.79" rx="1.56" />
        <rect width="3.12" height="32.62" x="11.57" y="6.31" rx="1.56" />
      </g>
    </svg>
  );
};

Shop.propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

Shop.defaultProps = {
  fill: '#FFF',
  className: '',
  onClick: null,
};

export default Shop;
