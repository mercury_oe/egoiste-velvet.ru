import React from 'react';
import PropTypes from 'prop-types';

const Shop = ({ fill, className }) => {
  return (
    <svg className={className} viewBox="0 0 23 29">
      <g fill={fill} fillRule="evenodd">
        <path d="M1.79 6.99a.9.9 0 0 1 .89-.83h17.69a.9.9 0 0 1 .88.83l1.7 17.08c.14.85 0 2.1-.81 3.14-.85 1.08-2.32 1.79-4.54 1.79H5.5c-2.27 0-3.76-.7-4.62-1.79a4.08 4.08 0 0 1-.84-3.15L1.79 7zm1.69 1.03L1.82 24.29l-.01.06c-.08.43 0 1.1.44 1.67.43.53 1.34 1.12 3.26 1.12H17.6c1.86 0 2.74-.58 3.16-1.12a2.22 2.22 0 0 0 .42-1.73L19.56 8.02H3.48z" />
        <path d="M7.46 5.12v5.39c0 .51-.4.93-.9.93-.48 0-.88-.42-.88-.93V4.88c.14-.84.58-2.04 1.47-3.04A5.45 5.45 0 0 1 11.37 0c1.93 0 3.3.8 4.22 1.84a6.27 6.27 0 0 1 1.48 3.2v5.47c0 .51-.4.93-.9.93a.91.91 0 0 1-.89-.93V5.1a4.4 4.4 0 0 0-1-2 3.7 3.7 0 0 0-2.9-1.25c-1.4 0-2.32.57-2.92 1.24-.6.68-.9 1.49-1 2.02z" />
      </g>
    </svg>
  );
};

Shop.propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
};

Shop.defaultProps = {
  fill: '#D0C2B3',
  className: '',
};

export default Shop;
