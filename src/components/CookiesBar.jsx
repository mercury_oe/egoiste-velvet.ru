import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { useIsDesktop } from '../utils/responsiveUtils';

function setCookie(name, value, props = {}) {
  const cookieProps = { ...props } || {};
  const { expires } = cookieProps;

  if (expires && typeof expires === 'number') {
    const date = new Date();
    cookieProps.expires = date.setTime(date.getTime() + expires * 1000);
  }
  if (expires && expires.toUTCString) {
    cookieProps.expires = expires.toUTCString();
  }

  const cookieValue = encodeURIComponent(value);
  let cookie = `${name}=${cookieValue}`;

  for (const key in cookieProps) {
    cookie += `; ${key}=${cookieProps[key]}`;
  }
  document.cookie = cookie;
}

function getCookie(name) {
  const matches = document.cookie.match(`(?:^|; )${name}=([^;]*)`);
  return matches ? decodeURIComponent(matches[1]) : null;
}

export default function CookiesBar({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { cookies },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              cookies {
                en {
                  desk
                  info
                  button
                }
                ru {
                  desk
                  info
                  button
                }
              }
            }
          }
        }
      }
    `,
  );

  const [show, toggle] = useState(false);
  useEffect(() => {
    const cookieSectionIsClosed = getCookie('cookie') === 'showed';
    toggle(!cookieSectionIsClosed);
  }, []);

  function handleClick() {
    setCookie('cookie', 'showed', { expires: 999999999 });
    toggle(false);
  }

  return (
    <>
      {show && (
        <div
          className="section py-5 fixed bottom-0 w-screen z-30"
          style={{
            background: 'rgba(135, 36, 164, .7)',
          }}
        >
          <div className="flex justify-between align-center w-full">
            <p className="text-white text-sm w-5/6 md:w-full">
              {cookies[locale].desk}{' '}
              <a target="_blank" href="#">
                {cookies[locale].info}
              </a>
            </p>
            <button onClick={handleClick} className="text-white font-bold text-sm">
              {cookies[locale].button}
            </button>
          </div>
        </div>
      )}
    </>
  );
}

CookiesBar.propTypes = {
  locale: PropTypes.string.isRequired,
};
