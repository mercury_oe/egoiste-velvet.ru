import React, { useContext } from 'react';
import './section-bar.css';
import { Context } from './Layout';

export default function ShopBar() {
  const { currentSection } = useContext(Context);

  return (
    <div className="section-bar flex flex-col justify-center align-center fixed z-10 h-screen top-0 py-5 md:py-8 lg:py-12 xl:py-16">
      <p className="section-bar__title text-brown-light text-tiny cursor-pointer">
        <span>{currentSection.title}</span>
      </p>
    </div>
  );
}
