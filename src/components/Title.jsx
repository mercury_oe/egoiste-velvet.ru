import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Title = ({ white, children, className: _className }) => {
  const textColor = white ? 'text-white' : 'text-purple';
  const className = classNames(
    textColor,
    'text-lg',
    'uppercase',
    'text-left',
    'leading-tight',
    _className,
  );

  return (
    <>
      <h2 className={className} dangerouslySetInnerHTML={{ __html: children }} />
    </>
  );
};

Title.propTypes = {
  white: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Title.defaultProps = {
  white: false,
  className: '',
};

export default Title;
