import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function CharacteristicItem({ className, label, value }) {
  return (
    <div className={classNames('flex', 'flex-col', 'md:flex-row', className)}>
      <span className="leading-tight text-base text-brown uppercase md:pr-6 md:w-1/2 md:text-right">
        {label}:
      </span>
      <span
        className="leading-tight text-base text-brown mt-1 md:mt-0 md:w-5/12"
        dangerouslySetInnerHTML={{ __html: value }}
      />
    </div>
  );
}

CharacteristicItem.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

CharacteristicItem.defaultProps = {
  className: '',
};
