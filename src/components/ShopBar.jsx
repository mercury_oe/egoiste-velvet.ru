import React from 'react';
import { Link } from 'react-scroll';
import Shop from './icons/Shop';
import './shop-bar.css';

export default function ShopBar() {
  return (
    <div className="shop-bar flex flex-col justify-end align-start fixed z-10 h-screen top-0 py-5 md:py-8 lg:py-12 xl:py-16">
      <Link
        to="partners"
        smooth
        className="shop-bar__buy text-brown-light text-tiny absolute cursor-pointer"
      >
        Купить
      </Link>
      <Link to="partners" smooth>
        <Shop className="shop-bar__icon w-6 cursor-pointer xl:w-8" fill="#D0C2B3" />
      </Link>
    </div>
  );
}
