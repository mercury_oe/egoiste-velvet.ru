import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import './loading.css';
import Logo from './icons/Logo';
import { Context } from './Layout';

export default function Loading() {
  const { isLoaded } = useContext(Context);
  const [show, setShow] = useState(true);

  useEffect(() => {
    if (isLoaded) {
      setTimeout(() => {
        setShow(false);
      }, 2000);
    }
  }, [isLoaded]);

  return (
    <>
      {show && (
        <article
          className={classNames(
            'loading-screen h-screen w-screen bg-gray-light left-0 top-0 z-100 overflow-hidden flex flex-col justify-around items-center py-24 loading',
            isLoaded ? 'opacity-0' : 'opacity-100',
            {
              fixed: true,
            },
          )}
        >
          <Logo className="w-1/4 md:w-24 lg:w-48" />
          <div className="w-full flex flex-col items-center">
            <PercentBar />
            <div style={{ height: 5 }} className="w-full line block relative">
              <span style={{ height: 5 }} className="w-full block bg-brown-lighter" />
              <span className="bar bg-purple w-10" />
            </div>
          </div>
        </article>
      )}
    </>
  );
}

function PercentBar() {
  const { length, progress } = useContext(Context);
  const value = Number((progress / length) * 100);

  return (
    <span className="text-base text-brown mb-12">
      {Number.isNaN(value) ? 0 : value.toFixed(0)}%
    </span>
  );
}
