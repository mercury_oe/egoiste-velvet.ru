import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useIsLaptop, useIsDesktop } from '../utils/responsiveUtils';
import './partner.css';

export default function Partner(props) {
  const { className, name, url, logos, width, height } = props;
  const isLaptop = useIsLaptop();
  const isDesktop = useIsDesktop();

  return (
    <div
      className={classNames(
        'partner',
        'relative',
        'w-1/2',
        // 'md:w-1/3',
        'md:justify-center',
        'md:text-center',
        // 'lg:w-1/3',
        'lg:flex',
        'lg:text-left',
        'lg:items-end',
        className,
      )}
    >
      <a
        href={url}
        target="_blank"
        rel="noopener noreferrer"
        className="partner__name cursor-pointer text-base uppercase text-brown lg:leading-none"
      >
        {name}
      </a>
      {(isLaptop || isDesktop) && (
        <div
          className="partner__image-wrapper"
          style={{
            width,
          }}
        >
          <img
            className="partner__image absolute bottom-0"
            style={{
              width,
              height,
            }}
            width={width}
            height={height}
            src={logos.x1}
            srcSet={`${logos.x2} 2x`}
            alt={name}
          />
        </div>
      )}
    </div>
  );
}

Partner.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  logos: PropTypes.shape({
    x1: PropTypes.string,
    x2: PropTypes.string,
  }).isRequired,
};

Partner.defaultProps = {
  className: '',
};
