import { useMediaQuery } from 'react-responsive';

export const useIsMobile = () => useMediaQuery({ maxWidth: 767 });
export const useIsTablet = () => useMediaQuery({ minWidth: 768, maxWidth: 1279 });
export const useIsLaptop = () => useMediaQuery({ minWidth: 1280, maxWidth: 1919 });
export const useIsDesktop = () => useMediaQuery({ minWidth: 1920 });

export const NotMobile = ({ children }) => {
  const isMobile = useIsMobile();
  return !isMobile ? children : null;
};

export const LaptopOrDesktop = ({ children }) => {
  const isDesktop = useIsDesktop();
  const isSmallDesktop = useIsLaptop();
  return isDesktop || isSmallDesktop ? children : null;
};

export const MobileOrTable = ({ children }) => {
  const isMobile = useIsMobile();
  const isTablet = useIsTablet();
  return isMobile || isTablet ? children : null;
};

export const Desktop = ({ children }) => {
  const isDesktop = useIsDesktop();
  return isDesktop ? children : null;
};

export const SmallDesktop = ({ children }) => {
  const isSmallDesktop = useIsLaptop();
  return isSmallDesktop ? children : null;
};

export const Tablet = ({ children }) => {
  const isTablet = useIsTablet();
  return isTablet ? children : null;
};

export const Mobile = ({ children }) => {
  const isMobile = useIsMobile();
  return isMobile ? children : null;
};

export const ResponsiveContainer = ({ mobile, tablet, laptop, desktop, children }) => {
  const isMobile = useIsMobile();
  const isTablet = useIsTablet();
  const isSmallDesktop = useIsLaptop();
  const isDesktop = useIsDesktop();

  if (mobile && isMobile) {
    return children;
  }

  if (tablet && isTablet) {
    return children;
  }

  if (laptop && isSmallDesktop) {
    return children;
  }

  if (desktop && isDesktop) {
    return children;
  }

  return null;
};
