import { useRef, useEffect } from 'react';

/**
 * @param {string} eventType
 * @param {Function} handler
 * @param {Element|Document|Window} element
 * @param {Object} options
 */
export default function useEventListener(eventType, handler, element, options = {}) {
  const saveHandler = useRef();

  useEffect(() => {
    saveHandler.current = handler;
  }, [handler]);

  useEffect(() => {
    const eventListener = (event) => saveHandler.current(event);

    if (element) {
      element.addEventListener(eventType, eventListener);
    }

    return () => {
      if (element) {
        element.removeEventListener(eventType, eventListener);
      }
    };
  }, [eventType, element, options]);
}
