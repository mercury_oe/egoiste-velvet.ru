import { useMemo, useState } from 'react';

export function detectRetinaDisplay() {
  return typeof window !== 'undefined' && window.devicePixelRatio >= 2;
}

export default function useLoadImages(imgs = [], setProgress) {
  // eslint-disable-next-line no-unused-vars
  const [loadedImgs, setLoadedImgs] = useState([]);
  const [complete, setComplete] = useState(false);

  const images = useMemo(
    () =>
      imgs.map((path, i) => {
        if (typeof Image !== 'undefined') {
          const img = new Image();
          img.src = path;
          img.onload = () => {
            loadedImgs.push(i);
            if (setProgress) {
              setProgress();
            }
            if (images.length === loadedImgs.length) {
              setComplete(true);
            }
          };
          return img;
        }
        return path;
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return complete;
}