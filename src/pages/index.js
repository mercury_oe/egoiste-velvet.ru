/* eslint-disable react/jsx-filename-extension */
import React from 'react';

import Layout from '../components/Layout';
import CharacteristicsGroundAndGrain from '../sections/CharacteristicsGroundAndGrain';
import CharacteristicsInstant from '../sections/CharacteristicsInstant';
import Partners from '../sections/Partners/Partners';
import Footer from '../sections/Footer';
import MainScreen from '../sections/MainScreen/MainScreen';
import GrainAndGround from '../sections/GrainAndGround';
import Quote from '../sections/Quote';
import GroundSequence from '../sections/GroundSequence';
import {
  Desktop,
  LaptopOrDesktop,
  Mobile,
  NotMobile,
  ResponsiveContainer,
  SmallDesktop,
  Tablet,
} from '../utils/responsiveUtils';
import CookiesBar from '../components/CookiesBar';
import InstantSequence from '../sections/InstantSequence';
import groundMob from '../images/ground/ground-mob.jpg';
import groundMobx2 from '../images/ground/ground-mob-x2.jpg';
import groundTab from '../images/ground/ground-tab.jpg';
import groundTabx2 from '../images/ground/ground-tab-x2.jpg';

import jarDeskX2 from '../images/sequences/jar/desk/x2';
import jarDeskx2Webp from '../images/sequences/jar/desk/x2/webp';
import jarDesk from '../images/sequences/jar/desk/x1';
import jarDeskWebp from '../images/sequences/jar/desk/x1/webp';
import jarLaptopX2 from '../images/sequences/jar/laptop/x2';
import jarLaptopx2Webp from '../images/sequences/jar/laptop/x2/webp';
import jarLaptop from '../images/sequences/jar/laptop/x1';
import jarLaptopWebp from '../images/sequences/jar/laptop/x1/webp';
import jarMobileX2 from '../images/sequences/jar/mobile/x2';
import jarMobilex2Webp from '../images/sequences/jar/mobile/x2/webp';
import jarMobile from '../images/sequences/jar/mobile/x1';
import jarMobileWebp from '../images/sequences/jar/mobile/x1/webp';

import instantMob from '../images/instant/instant-mob.jpg';
import instantMobx2 from '../images/instant/instant-mob-x2.jpg';
import instantTab from '../images/instant/instant-tab.jpg';
import instantTabx2 from '../images/instant/instant-tab-x2.jpg';
import { detectRetinaDisplay } from '../utils/useLoadImages';
import JarSequence from '../sections/JarSequence';
import ShopBar from '../components/ShopBar';
import Loading from '../components/Loading';
import SectionBar from '../components/SectionBar';

const IndexPage = () => {
  return (
    <Layout>
      <NotMobile>
        <Loading />
      </NotMobile>
      <MainScreen locale="ru" />
      <GrainAndGround locale="ru" />
      <LaptopOrDesktop>
        <GroundSequence locale="ru" />
      </LaptopOrDesktop>
      <Mobile>
        <img
          src={groundMob}
          srcSet={`${groundMobx2} 2x`}
          className="mx-auto mb-12"
          alt="Молотый и зерновой"
        />
      </Mobile>
      <Tablet>
        <img
          src={groundTab}
          srcSet={`${groundTabx2} 2x`}
          className="mx-auto"
          alt="Молотый и зерновой"
        />
      </Tablet>
      <Quote locale="ru" />
      <LaptopOrDesktop>
        <InstantSequence locale="ru" />
      </LaptopOrDesktop>
      <Mobile>
        <img
          src={instantMob}
          srcSet={`${instantMobx2} 2x`}
          className="mt-16 mb-16 mx-auto"
          alt="Растворимый"
        />
      </Mobile>
      <Tablet>
        <img
          src={instantTab}
          srcSet={`${instantTabx2} 2x`}
          className="mt-20 mb-20 mx-auto"
          alt="Растворимый"
        />
      </Tablet>
      <CharacteristicsGroundAndGrain locale="ru" />
      <Mobile>
        <JarSequence
          locale="ru"
          width={174}
          images={detectRetinaDisplay() ? jarMobileX2 : jarMobile}
          imagesWebp={detectRetinaDisplay() ? jarMobilex2Webp : jarMobileWebp}
        />
      </Mobile>
      <Tablet>
        <JarSequence
          locale="ru"
          width={190}
          images={detectRetinaDisplay() ? jarLaptopX2 : jarLaptop}
          imagesWebp={detectRetinaDisplay() ? jarLaptopx2Webp : jarLaptopWebp}
        />
      </Tablet>
      <SmallDesktop>
        <JarSequence
          locale="ru"
          width={261}
          images={detectRetinaDisplay() ? jarLaptopX2 : jarLaptop}
          imagesWebp={detectRetinaDisplay() ? jarLaptopx2Webp : jarLaptopWebp}
        />
      </SmallDesktop>
      <Desktop>
        <JarSequence
          locale="ru"
          width={375}
          images={detectRetinaDisplay() ? jarDeskX2 : jarDesk}
          imagesWebp={detectRetinaDisplay() ? jarDeskx2Webp : jarDeskWebp}
        />
      </Desktop>
      <CharacteristicsInstant locale="ru" />
      <Partners locale="ru" />
      <Footer locale="ru" />
      <ResponsiveContainer tablet laptop desktop>
        <ShopBar locale="ru" />
      </ResponsiveContainer>
      <ResponsiveContainer tablet laptop desktop>
        <SectionBar locale="ru" />
      </ResponsiveContainer>
      <CookiesBar locale="ru" />
    </Layout>
  );
};

export default IndexPage;
