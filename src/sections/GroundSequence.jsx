import React, { useContext, useEffect, useRef } from 'react';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import laptopImages from '../images/sequences/ground/laptop';
import laptopX2Images from '../images/sequences/ground/laptop/x2';
import deskImages from '../images/sequences/ground/desk';
import deskX2Images from '../images/sequences/ground/desk/x2';
import { detectRetinaDisplay } from '../utils/useLoadImages';
import { useIsLaptop } from '../utils/responsiveUtils';
import Sequence from '../components/Sequence';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;
export default function GroundSequence({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { instantCoffeeSequence },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              instantCoffeeSequence {
                en {
                  sectionName
                }
                ru {
                  sectionName
                }
              }
            }
          }
        }
      }
    `,
  );

  const images = useIsLaptop() ? laptopImages : deskImages;
  const imagesX2 = useIsLaptop() ? laptopX2Images : deskX2Images;
  const isRetina = detectRetinaDisplay();
  const imgs = isRetina ? imagesX2 : images;
  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration =
        ref.current.current.offsetHeight * 2 + document.documentElement.clientHeight / 2 + 500;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.ground-sequence',
        duration,
      })
        .addTo(controller)

      scene.on('enter', () => {
        setCurrentSection({
          title: instantCoffeeSequence[locale].sectionName,
        });
      });
    }
  }, [instantCoffeeSequence, locale, setCurrentSection]);

  return <Sequence ref={ref} className="ground-sequence" images={imgs} />;
}
