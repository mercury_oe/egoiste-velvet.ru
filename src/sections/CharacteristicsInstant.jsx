import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef } from 'react';
import classNames from 'classnames';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import Title from '../components/Title';
import CharacteristicItem from '../components/CharacteristicItem';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;

export default function CharacteristicsInstant({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { instantCharacteristics, instant },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              instantCharacteristics {
                en {
                  title
                  structure {
                    key
                    value
                  }
                  region {
                    key
                    value
                  }
                  treatment {
                    key
                    value
                  }
                  roasting {
                    key
                    value
                  }
                  production {
                    key
                    value
                  }
                  desc {
                    key
                    value
                  }
                }
                ru {
                  title
                  structure {
                    key
                    value
                  }
                  region {
                    key
                    value
                  }
                  treatment {
                    key
                    value
                  }
                  roasting {
                    key
                    value
                  }
                  production {
                    key
                    value
                  }
                  desc {
                    key
                    value
                  }
                }
              }
              instant {
                en {
                  sectionName
                  title
                }
                ru {
                  sectionName
                  title
                }
              }
            }
          }
        }
      }
    `,
  );

  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.instant-characteristics',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: instant[locale].sectionName,
        });
      });

      new ScrollMagic.Scene({
        triggerElement: '.title-instant',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.title-instant', 'visible')
        .addTo(controller);

      new Array(6).fill(null).map((i, index) => {
        new ScrollMagic.Scene({
          triggerElement: `.instant-item-${index + 1}`,
          triggerHook: 0.95,
          offset: 0,
        })
          .setClassToggle(`.instant-item-${index + 1}`, 'visible')
          .addTo(controller);
      });
    }
  }, [instant, locale, setCurrentSection]);

  return (
    <div
      ref={ref}
      className="instant-characteristics sec pt-12 pb-16 md:pb-32 lg:min-h-screen lg:pb-0 lg:pt-16 xl:pt-20 lg:pt-8 xl:pt-10"
    >
      <Title className="title-instant reveal">{instantCharacteristics[locale].title}</Title>
      <CharacteristicItem
        className={classNames('mt-8 md:mt-16 lg:mt-32 reveal instant-item-1')}
        label={instantCharacteristics[locale].structure.key}
        value={instantCharacteristics[locale].structure.value}
      />
      <CharacteristicItem
        className={classNames('mt-8 md:mt-5 lg:mt-8 reveal instant-item-2')}
        label={instantCharacteristics[locale].region.key}
        value={instantCharacteristics[locale].region.value}
      />
      <CharacteristicItem
        className={classNames('mt-8 md:mt-5 lg:mt-8 reveal instant-item-3')}
        label={instantCharacteristics[locale].treatment.key}
        value={instantCharacteristics[locale].treatment.value}
      />
      <CharacteristicItem
        className={classNames('mt-8 md:mt-5 lg:mt-8 reveal instant-item-4')}
        label={instantCharacteristics[locale].roasting.key}
        value={instantCharacteristics[locale].roasting.value}
      />
      <CharacteristicItem
        className={classNames('mt-8 md:mt-5 lg:mt-8 reveal instant-item-5')}
        label={instantCharacteristics[locale].production.key}
        value={instantCharacteristics[locale].production.value}
      />
      <CharacteristicItem
        className={classNames('mt-8 md:mt-5 lg:mt-8 reveal instant-item-6')}
        label={instantCharacteristics[locale].desc.key}
        value={instantCharacteristics[locale].desc.value}
      />
    </div>
  );
}

CharacteristicsInstant.propTypes = {
  locale: PropTypes.string.isRequired,
};
