import ozonX1 from '../../images/ozon-x1.png';
import ozonX2 from '../../images/ozon-x2.png';

import ashanX1 from '../../images/ashan-x1.png';
import ashanX2 from '../../images/ashan-x2.png';

import komusX1 from '../../images/komus-x1.png';
import komusX2 from '../../images/komus-x2.png';

import lentaX1 from '../../images/lenta-x1.png';
import lentaX2 from '../../images/lenta-x2.png';

import okayX1 from '../../images/okay-x1.png';
import okayX2 from '../../images/okay-x2.png';

import utkonosX1 from '../../images/untkonos-x1.png';
import utkonosX2 from '../../images/untkonos-x2.png';

export const ozonLogos = {
  x1: ozonX1,
  x2: ozonX2,
};
export const ashanLogos = {
  x1: ashanX1,
  x2: ashanX2,
};
export const komusLogos = {
  x1: komusX1,
  x2: komusX2,
};
export const lentaLogos = {
  x1: lentaX1,
  x2: lentaX2,
};
export const okayLogos = {
  x1: okayX1,
  x2: okayX2,
};
export const utkonosLogos = {
  x1: utkonosX1,
  x2: utkonosX2,
};