import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef } from 'react';
import { Element } from 'react-scroll';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import Title from '../../components/Title';
import Partner from '../../components/Partner';
import { ashanLogos, lentaLogos, okayLogos, ozonLogos, utkonosLogos } from './images';
import { Context } from '../../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;

export default function Partners({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { partners },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              partners {
                en {
                  sectionName
                  title
                }
                ru {
                  sectionName
                  title
                }
              }
            }
          }
        }
      }
    `,
  );

  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.partners-section',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: partners[locale].sectionName,
        });
      });

      new ScrollMagic.Scene({
        triggerElement: '.title-partners',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.title-partners', 'visible')
        .addTo(controller);

      new Array(5).fill(null).map((i, index) => {
        new ScrollMagic.Scene({
          triggerElement: `.partners-item-${index + 1}`,
          triggerHook: 0.95,
          offset: 0,
        })
          .setClassToggle(`.partners-item-${index + 1}`, 'visible')
          .addTo(controller);
      });
    }
  }, [locale, partners, setCurrentSection]);

  return (
    <Element name="partners">
      <section
        ref={ref}
        className="partners-section section pb-16 md:pb-24 lg:w-screen lg:h-screen lg:pb-0 lg:pt-16 xl:pt-20 lg:pt-8 xl:pt-10"
      >
        <Title className="title-partners reveal">{partners[locale].title}</Title>
        <div className="mt-16 flex flex-wrap justify-between md:w-3/4 md:mx-auto md:mt-32 md:mb-32 lg:mb-0 lg:w-10/12 lg:mt-48 xl:w-8/12 xl:mx-auto">
          <Partner
            className="partners-item-1 reveal"
            name="ozon"
            logos={ozonLogos}
            width={225}
            height={45}
            url="https://www.ozon.ru/category/produkty-pitaniya-9200/?text=egoiste+velvet&from_global=true"
          />
          <Partner
            className="partners-item-2 reveal"
            name="ашан"
            logos={ashanLogos}
            width={202}
            height={62}
            url="https://www.auchan.ru/pokupki/yandexsearch/result/index/q/egoiste%20velvet"
          />
          <Partner
            className="mt-8 md:mt-0 partners-item-3 reveal"
            name="уотконос"
            logos={utkonosLogos}
            width={202}
            height={52}
            url="https://www.utkonos.ru/search?query=egoiste+velvet&search_id="
          />
          <Partner
            className="mt-8 md:mt-0 md:mt-6 lg:mt-16 xl:mt-16 partners-item-4 reveal"
            name="окей"
            logos={okayLogos}
            width={193}
            height={55}
            url="https://www.okeydostavka.ru/webapp/wcs/stores/servlet/SearchDisplay?categoryId=&storeId=10151&catalogId=12051&langId=-20&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&pageSize=72&searchTerm=%D0%BA%D0%BE%D1%84%D0%B5+egoiste+velvet"
          />
          {/* <Partner */}
          {/*  className="mt-8 md:mt-0 md:mt-6 lg:mt-16 xl:mt-16 partners-item-5 reveal" */}
          {/*  name="комус" */}
          {/*  logos={komusLogos} */}
          {/*  width={208} */}
          {/*  height={57} */}
          {/* /> */}
          <Partner
            className="mt-8 md:mt-0 md:mt-6 lg:mt-16 xl:mt-16 partners-item-5 reveal"
            name="лента"
            logos={lentaLogos}
            width={212}
            height={68}
            url="https://lenta.com/search/?searchText=egoiste%20velvet"
          />
        </div>
      </section>
    </Element>
  );
}

Partners.propTypes = {
  locale: PropTypes.string.isRequired,
};
