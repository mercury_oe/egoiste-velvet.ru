import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef } from 'react';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import Title from '../components/Title';
import Quotes from '../components/icons/Quotes';
import { LaptopOrDesktop, MobileOrTable } from '../utils/responsiveUtils';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;

const Quote = ({ locale }) => {
  const {
    site: {
      siteMetadata: {
        sections: { quote, instantCoffeeSequence },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              quote {
                en {
                  title
                  desc
                }
                ru {
                  title
                  desc
                }
              }
              instantCoffeeSequence {
                en {
                  sectionName
                }
                ru {
                  sectionName
                }
              }
            }
          }
        }
      }
    `,
  );
  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.quote',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: instantCoffeeSequence[locale].sectionName,
        });
      });

      new ScrollMagic.Scene({
        triggerElement: '.quote',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.quote', 'visible')
        .addTo(controller);

      new ScrollMagic.Scene({
        triggerElement: '.quote-title',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.quote-title', 'visible')
        .addTo(controller);

      new ScrollMagic.Scene({
        triggerElement: '.quotes',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.quotes', 'visible')
        .addTo(controller);
    }
  }, []);

  return (
    <div ref={ref} className="section quote lg:pr-16 xl:pr-24 lg:mb-32">
      <LaptopOrDesktop>
        <Quotes className="mb-6 quotes reveal" width={98} height={64} />
      </LaptopOrDesktop>
      <MobileOrTable>
        <Quotes className="mb-6 quotes reveal" />
      </MobileOrTable>
      <p className="leading-tight text-brown mb-6 text-base text-brown md:w-1/3 md:float-left md:mr-10 md:mr-16 md:mt-3 lg:mr-5 lg:w-1/4 reveal quote">
        {quote[locale].desc}
      </p>
      <Title className="reveal quote-title lg:text-md xl:text-md quote-text">
        {quote[locale].title}
      </Title>
    </div>
  );
};

export default Quote;

Quote.propTypes = {
  locale: PropTypes.string.isRequired,
};
