import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-scroll';
import { graphql, useStaticQuery } from 'gatsby';
import Logo from '../components/icons/Logo';

export default function Footer({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { footer },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              footer {
                en {
                  questions
                  termOfUse
                }
                ru {
                  questions
                  termOfUse
                }
              }
            }
          }
        }
      }
    `,
  );
  return (
    <footer className="section pb-8 md:flex md:justify-between lg:-mt-12 xl:-mt-16 lg:pb-8 xl:pb-12">
      <div className="md:flex md:w-full">
        <div className="md:flex md:mr-6">
          <span className="text-tiny text-brown font-bold">
            {footer[locale].questions}:{' '}
            <a
              className="hover:text-brown-light text-tiny text-brown font-bold"
              href="mailto:info@horsgroup.com"
            >
              info@horsgroup.com
            </a>
          </span>
        </div>
        <a
          className="hover:text-brown-light block text-tiny text-brown mt-3 md:mt-0 font-bold"
          href="#"
        >
          {footer[locale].termOfUse}
        </a>
      </div>
      <div className="w-1/4 md:flex justify-end">
        <Link to="main" smooth className="flex justify-end">
          <Logo style={{ width: 88, height: 24 }} className="w-full mt-6 md:mt-0 md:w-1/2" />
        </Link>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  locale: PropTypes.string.isRequired,
};
