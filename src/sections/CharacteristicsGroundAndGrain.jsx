import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef } from 'react';
import classNames from 'classnames';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import Title from '../components/Title';
import CharacteristicItem from '../components/CharacteristicItem';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;

export default function CharacteristicsGroundAndGrain({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { groundAndGrainCharacteristics, groundCoffeeSequence },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              groundAndGrainCharacteristics {
                en {
                  title
                  structure {
                    key
                    value
                  }
                  region {
                    key
                    value
                  }
                  treatment {
                    key
                    value
                  }
                  roasting {
                    key
                    value
                  }
                  production {
                    key
                    value
                  }
                  desc {
                    key
                    value
                  }
                }
                ru {
                  title
                  structure {
                    key
                    value
                  }
                  region {
                    key
                    value
                  }
                  treatment {
                    key
                    value
                  }
                  roasting {
                    key
                    value
                  }
                  production {
                    key
                    value
                  }
                  desc {
                    key
                    value
                  }
                }
              }
              groundCoffeeSequence {
                en {
                  sectionName
                }
                ru {
                  sectionName
                }
              }
            }
          }
        }
      }
    `,
  );
  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.ground-characteristics',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: groundCoffeeSequence[locale].sectionName,
        });
      });

      new ScrollMagic.Scene({
        triggerElement: '.title-gng',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.title-gng', 'visible')
        .addTo(controller);

      new ScrollMagic.Scene({
        triggerElement: '.gng-item',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.gng-item', 'visible')
        .addTo(controller);
    }
  }, [groundCoffeeSequence, locale, setCurrentSection]);

  return (
    <div
      ref={ref}
      className="sec ground-characteristics pb-16 md:pb-24 flex flex-col lg:min-h-screen lg:pb-0 lg:pt-16 xl:pt-20 lg:pt-8 xl:pt-10"
    >
      <Title className="title-gng reveal">{groundAndGrainCharacteristics[locale].title}</Title>
      <div className="reveal gng-item">
        <CharacteristicItem
          className={classNames('mt-8 md:mt-16 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].structure.key}
          value={groundAndGrainCharacteristics[locale].structure.value}
        />
        <CharacteristicItem
          className={classNames('mt-8 md:mt-5 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].region.key}
          value={groundAndGrainCharacteristics[locale].region.value}
        />
        <CharacteristicItem
          className={classNames('mt-8 md:mt-5 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].treatment.key}
          value={groundAndGrainCharacteristics[locale].treatment.value}
        />
        <CharacteristicItem
          className={classNames('mt-8 md:mt-5 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].treatment.key}
          value={groundAndGrainCharacteristics[locale].treatment.value}
        />
        <CharacteristicItem
          className={classNames('mt-8 md:mt-5 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].production.key}
          value={groundAndGrainCharacteristics[locale].production.value}
        />
        <CharacteristicItem
          className={classNames('mt-8 md:mt-5 lg:mt-8')}
          label={groundAndGrainCharacteristics[locale].desc.key}
          value={groundAndGrainCharacteristics[locale].desc.value}
        />
      </div>
    </div>
  );
}

CharacteristicsGroundAndGrain.propTypes = {
  locale: PropTypes.string.isRequired,
};
