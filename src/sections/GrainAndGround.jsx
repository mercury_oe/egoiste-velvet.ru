import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef } from 'react';
import ScrollMagic from 'scrollmagic-with-ssr';
import { graphql, useStaticQuery } from 'gatsby';
import Title from '../components/Title';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;

const GrainAndGround = ({ locale }) => {
  const {
    site: {
      siteMetadata: {
        sections: { grainAndGround },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              grainAndGround {
                en {
                  sectionName
                  title
                  desc
                }
                ru {
                  sectionName
                  title
                  desc
                }
              }
            }
          }
        }
      }
    `,
  );
  const { setCurrentSection } = useContext(Context);
  const section = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = section.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.grain-ground',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: grainAndGround[locale].sectionName,
        });
      });

      new ScrollMagic.Scene({
        triggerElement: '.title',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.title', 'visible')
        .addTo(controller);

      new ScrollMagic.Scene({
        triggerElement: '.paragraph',
        triggerHook: 0.95,
        offset: 0,
      })
        .setClassToggle('.paragraph', 'visible')
        .addTo(controller);
    }
  }, [grainAndGround, locale, setCurrentSection]);

  return (
    <article
      ref={section}
      className="section grain-ground pt-8 md:flex md:flex-col md:pt-20 lg:pt-24"
    >
      <Title className="mb-5 md:mb-10 lg:mb-16 lg:w-4/6 reveal title">
        {grainAndGround[locale].title}
      </Title>
      <p
        className="leading-tight text-brown text-base md:w-1/2 md:self-end reveal paragraph"
        dangerouslySetInnerHTML={{ __html: grainAndGround[locale].desc }}
      />
    </article>
  );
};

export default GrainAndGround;

GrainAndGround.propTypes = {
  locale: PropTypes.string.isRequired,
};
