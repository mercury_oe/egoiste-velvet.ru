import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import Slider from 'rc-slider';
import { graphql, useStaticQuery } from 'gatsby';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';
import Title from '../components/Title';
import useLoadImages from '../utils/useLoadImages';
import 'rc-slider/assets/index.css';
import '../styles/slider.css';
import { Context } from '../components/Layout';
import ScrollMagic from 'scrollmagic-with-ssr';

function canUseWebP() {
  if (typeof document !== 'undefined') {
    const elem = document.createElement('canvas');

    if (elem.getContext && elem.getContext('2d')) {
      return elem.toDataURL('image/webp').indexOf('data:image/webp') === 0;
    }
  }

  return false;
}
const controller = ScrollMagic ? new ScrollMagic.Controller() : null;
export default function JarSequence({ width, locale, images, imagesWebp }) {
  const {
    site: {
      siteMetadata: {
        sections: { instant },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              instant {
                en {
                  sectionName
                  title
                }
                ru {
                  sectionName
                  title
                }
              }
            }
          }
        }
      }
    `,
  );
  const [value, setValue] = useState(0);
  const handleChange = useCallback(
    throttle(val => {
      setValue(val);
    }, 50),
    [],
  );

  const context = useContext(Context);
  const { setProgress, setLength } = context;
  const imgsIsLoaded = useLoadImages(canUseWebP() ? imagesWebp : images, setProgress);

  useEffect(() => {
    if (context && setLength) {
      setLength(v => v + images.length);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.instant-section',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: instant[locale].sectionName,
        });
      });
    }
  }, [instant, locale, setCurrentSection]);
  const className = 'inline-block select-none';
  const useWebp = canUseWebP();
  return (
    <article
      ref={ref}
      className="w-screen h-screen max-h-screen flex flex-col items-center justify-between py-12 lg:py-16 xl:py-20 lg:py-8 xl:py-10 instant-section"
    >
      <Title className="sec text-center" white>
        {instant[locale].title}
      </Title>
      {useWebp ? (
        <img
          className={className}
          src={imagesWebp[value]}
          style={{ width }}
          alt={instant[locale].title}
        />
      ) : (
        <img
          className={className}
          src={images[value]}
          style={{ width }}
          alt={instant[locale].title}
        />
      )}
      {imgsIsLoaded && (
        <Slider
          value={value}
          step={1}
          min={0}
          max={images.length - 1}
          className="slider"
          onChange={handleChange}
          marks={{ 1: '', [images.length - 1]: '' }}
        />
      )}
    </article>
  );
}

JarSequence.propTypes = {
  width: PropTypes.number.isRequired,
  locale: PropTypes.string.isRequired,
  images: PropTypes.arrayOf().isRequired,
  imagesWebp: PropTypes.arrayOf().isRequired,
};
