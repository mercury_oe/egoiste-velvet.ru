import React, { useContext, useEffect, useRef } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import ScrollMagic from 'scrollmagic-with-ssr';
import laptopImages from '../images/sequences/instant/laptop';
import laptopX2Images from '../images/sequences/instant/laptop/x2';
import deskImages from '../images/sequences/instant/desk';
import deskX2Images from '../images/sequences/instant/desk/x2';
import { detectRetinaDisplay } from '../utils/useLoadImages';
import { useIsLaptop } from '../utils/responsiveUtils';
import Sequence from '../components/Sequence';
import { Context } from '../components/Layout';

const controller = ScrollMagic ? new ScrollMagic.Controller() : null;
export default function GroundSequence({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { groundCoffeeSequence },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              groundCoffeeSequence {
                en {
                  sectionName
                }
                ru {
                  sectionName
                }
              }
            }
          }
        }
      }
    `,
  );

  const images = useIsLaptop() ? laptopImages : deskImages;
  const imagesX2 = useIsLaptop() ? laptopX2Images : deskX2Images;
  const isRetina = detectRetinaDisplay();
  const imgs = isRetina ? imagesX2 : images;
  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration =
        ref.current.current.offsetHeight * 2 * 4 + document.documentElement.clientHeight / 2;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.instant-sequence',
        duration,
      })
        .addTo(controller)

      scene.on('enter', () => {
        setCurrentSection({
          title: groundCoffeeSequence[locale].sectionName,
        });
      });
    }
  }, [groundCoffeeSequence, locale, setCurrentSection]);

  return <Sequence ref={ref} tastes images={imgs} className="instant-sequence" />;
}
