import PropTypes from 'prop-types';
import React, { useCallback, useContext, useEffect, useRef } from 'react';
import { Element, Link } from 'react-scroll';
import { graphql, useStaticQuery } from 'gatsby';
import { useMediaQuery } from 'react-responsive';
import classNames from 'classnames';
import Logo from '../../components/icons/Logo';
import LanguageToggle from '../../components/LanguageToggle';
import Shop from '../../components/icons/Shop';
import './main-screen.css';
import MainScreenVideo from './MainScreenVideo';
import { Mobile, ResponsiveContainer } from '../../utils/responsiveUtils';
import Sound from '../../components/icons/Sound';
import { Context } from '../../components/Layout';
import ScrollMagic from 'scrollmagic-with-ssr';

const useIsMobile = () => useMediaQuery({ maxWidth: 1023 });
const useIsTablet = () => useMediaQuery({ minWidth: 768, maxWidth: 768 });

const NotMobile = ({ children }) => {
  const isMobile = useIsMobile();
  return !isMobile ? children : null;
};

const MobileOrTable = ({ children }) => {
  const isMobile = useIsMobile();
  const isTablet = useIsTablet();
  return isMobile || isTablet ? children : null;
};
const controller = ScrollMagic ? new ScrollMagic.Controller() : null;
export default function MainScreen({ locale }) {
  const {
    site: {
      siteMetadata: {
        sections: { main },
      },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            sections {
              main {
                en {
                  buy
                }
                ru {
                  buy
                }
              }
            }
          }
        }
      }
    `,
  );
  const videoRef = useRef(null);
  const handleSoundToggle = useCallback(() => {
    if (videoRef.current) {
      videoRef.current.videoRef.current.muted = !videoRef.current.videoRef.current.muted;
    }
  }, []);

  const { setCurrentSection } = useContext(Context);
  const ref = useRef(null);

  useEffect(() => {
    if (controller) {
      const duration = ref.current.offsetHeight;
      const scene = new ScrollMagic.Scene({
        triggerElement: '.main-screen',
        duration,
      }).addTo(controller);
      scene.on('enter', () => {
        setCurrentSection({
          title: '',
        });
      });
    }
  }, [locale, setCurrentSection]);

  return (
    <section ref={ref} className="main-screen w-screen h-screen relative md:h-auto xl:h-screen">
      <Element
        name="main"
        className="section flex flex-col justify-between w-full h-screen relative z-10 py-8 md:py-8 md:h-auto md:absolute md:inset-0 lg:h-screen lg:py-12 xl:py-16"
      >
        <div className="flex justify-between align-center">
          <Logo fill="#FFF" className="h-full w-1/4 md:w-24 lg:w-48" />
          <Mobile>
            <LanguageToggle />
          </Mobile>
        </div>

        <div className="flex justify-between align-center relative">
          <ResponsiveContainer desktop laptop tablet>
            <LanguageToggle />
          </ResponsiveContainer>
          <Sound
            onClick={handleSoundToggle}
            className={classNames('main-screen__sound-button', {
              'main-screen__sound-button':
                videoRef.current && !videoRef.current.videoRef.current.muted,
            })}
          />
          <Mobile>
            <div className="flex items-center main-screen__button-wrapper">
              <span className="main-screen__buy-mobile" />
              <Link smooth to="partners" className="text-base text-white">
                {main[locale].buy}
              </Link>
            </div>
          </Mobile>
          <Mobile>
            <Link smooth to="partners">
              <Shop fill="#fff" className="w-5" />
            </Link>
          </Mobile>
        </div>
      </Element>
      <NotMobile>
        <MainScreenVideo ref={videoRef} />
      </NotMobile>
    </section>
  );
}

MainScreen.propTypes = {
  locale: PropTypes.string.isRequired,
};
