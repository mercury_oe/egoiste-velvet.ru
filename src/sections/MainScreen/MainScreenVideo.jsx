import React, {
  forwardRef,
  useContext,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import video from '../../video/main-video.mp4';
import { Context } from '../../components/Layout';
import { useIsMobile } from '../../utils/responsiveUtils';

const MainScreenVideo = forwardRef((props, ref) => {
  const videoRef = useRef(null);
  const { length, progress } = useContext(Context);
  const [render, setRender] = useState(false);
  const isMobile = useIsMobile();

  useEffect(() => {
    if (videoRef.current && length === progress) {
      videoRef.current.play();
    }
  }, [length, progress]);

  useEffect(() => {
    if (!isMobile) {
      setTimeout(() => {
        setRender(true);
      });
    }
  }, [isMobile]);

  useImperativeHandle(ref, () => ({
    videoRef,
  }));

  return (
    <>
      <video
        ref={videoRef}
        autoPlay
        loop
        muted
        preload="auto"
        className="w-screen absolute top-0 left-0 z-0 object-fill"
      >
        {render && <source src={video} type="video/mp4" />}
      </video>
    </>
  );
});

export default MainScreenVideo;
