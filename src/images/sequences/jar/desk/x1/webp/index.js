import jar0 from './jar_00000.webp';
import jar2 from './jar_00002.webp';
import jar4 from './jar_00004.webp';
import jar6 from './jar_00006.webp';
import jar8 from './jar_00008.webp';
import jar10 from './jar_00010.webp';
import jar12 from './jar_00012.webp';
import jar14 from './jar_00014.webp';
import jar16 from './jar_00016.webp';
import jar18 from './jar_00018.webp';
import jar20 from './jar_00020.webp';
import jar22 from './jar_00022.webp';
import jar24 from './jar_00024.webp';
import jar26 from './jar_00026.webp';
import jar28 from './jar_00028.webp';
import jar30 from './jar_00030.webp';
import jar32 from './jar_00032.webp';
import jar34 from './jar_00034.webp';
import jar36 from './jar_00036.webp';
import jar38 from './jar_00038.webp';
import jar40 from './jar_00040.webp';
import jar42 from './jar_00042.webp';
import jar44 from './jar_00044.webp';
import jar46 from './jar_00046.webp';
import jar48 from './jar_00048.webp';
import jar50 from './jar_00050.webp';
import jar52 from './jar_00052.webp';
import jar54 from './jar_00054.webp';
import jar56 from './jar_00056.webp';
import jar58 from './jar_00058.webp';
import jar60 from './jar_00060.webp';
import jar62 from './jar_00062.webp';
import jar64 from './jar_00064.webp';
import jar66 from './jar_00066.webp';
import jar68 from './jar_00068.webp';
import jar70 from './jar_00070.webp';
import jar72 from './jar_00072.webp';
import jar74 from './jar_00074.webp';
import jar76 from './jar_00076.webp';
import jar78 from './jar_00078.webp';
import jar80 from './jar_00080.webp';
import jar82 from './jar_00082.webp';
import jar84 from './jar_00084.webp';
import jar86 from './jar_00086.webp';
import jar88 from './jar_00088.webp';

export default [
  jar0,
  jar2,
  jar4,
  jar6,
  jar8,
  jar10,
  jar12,
  jar14,
  jar16,
  jar18,
  jar20,
  jar22,
  jar24,
  jar26,
  jar28,
  jar30,
  jar32,
  jar34,
  jar36,
  jar38,
  jar40,
  jar42,
  jar44,
  jar46,
  jar48,
  jar50,
  jar52,
  jar54,
  jar56,
  jar58,
  jar60,
  jar62,
  jar64,
  jar66,
  jar68,
  jar70,
  jar72,
  jar74,
  jar76,
  jar78,
  jar80,
  jar82,
  jar84,
  jar86,
  jar88,
];
