import jar0 from './jar_00000.png';
import jar2 from './jar_00002.png';
import jar4 from './jar_00004.png';
import jar6 from './jar_00006.png';
import jar8 from './jar_00008.png';
import jar10 from './jar_00010.png';
import jar12 from './jar_00012.png';
import jar14 from './jar_00014.png';
import jar16 from './jar_00016.png';
import jar18 from './jar_00018.png';
import jar20 from './jar_00020.png';
import jar22 from './jar_00022.png';
import jar24 from './jar_00024.png';
import jar26 from './jar_00026.png';
import jar28 from './jar_00028.png';
import jar30 from './jar_00030.png';
import jar32 from './jar_00032.png';
import jar34 from './jar_00034.png';
import jar36 from './jar_00036.png';
import jar38 from './jar_00038.png';
import jar40 from './jar_00040.png';
import jar42 from './jar_00042.png';
import jar44 from './jar_00044.png';
import jar46 from './jar_00046.png';
import jar48 from './jar_00048.png';
import jar50 from './jar_00050.png';
import jar52 from './jar_00052.png';
import jar54 from './jar_00054.png';
import jar56 from './jar_00056.png';
import jar58 from './jar_00058.png';
import jar60 from './jar_00060.png';
import jar62 from './jar_00062.png';
import jar64 from './jar_00064.png';
import jar66 from './jar_00066.png';
import jar68 from './jar_00068.png';
import jar70 from './jar_00070.png';
import jar72 from './jar_00072.png';
import jar74 from './jar_00074.png';
import jar76 from './jar_00076.png';
import jar78 from './jar_00078.png';
import jar80 from './jar_00080.png';
import jar82 from './jar_00082.png';
import jar84 from './jar_00084.png';
import jar86 from './jar_00086.png';
import jar88 from './jar_00088.png';

export default [
  jar0,
  jar2,
  jar4,
  jar6,
  jar8,
  jar10,
  jar12,
  jar14,
  jar16,
  jar18,
  jar20,
  jar22,
  jar24,
  jar26,
  jar28,
  jar30,
  jar32,
  jar34,
  jar36,
  jar38,
  jar40,
  jar42,
  jar44,
  jar46,
  jar48,
  jar50,
  jar52,
  jar54,
  jar56,
  jar58,
  jar60,
  jar62,
  jar64,
  jar66,
  jar68,
  jar70,
  jar72,
  jar74,
  jar76,
  jar78,
  jar80,
  jar82,
  jar84,
  jar86,
  jar88,
];
