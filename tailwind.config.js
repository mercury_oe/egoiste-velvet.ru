module.exports = {
  theme: {
    extend: {},
    colors: {
      'gray-light': '#f5f4f2',
      purple: '#693FB0',
      'purple-light': '#A988EC',
      brown: '#9B8268',
      'brown-light': '#D0C2B3',
      'brown-lighter': '#EAE5E0',
      white: '#FFF',
    },
    fontSize: {
      sm: '14px',
      tiny: '16px',
      base: '18px',
      lg: '28px',
    },
    screens: {
      sm: '640px', // => @media (min-width: 640px) { ... }
      md: '1024px', // => @media (min-width: 1024px) { ... }
      lg: '1280px', // => @media (min-width: 1280px) { ... }
      xl: '1920px', // => @media (min-width: 1920px) { ... }
    },
  },
  variants: {},
  plugins: [],
};
